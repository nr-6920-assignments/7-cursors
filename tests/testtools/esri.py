import arcpy

def compare_rasters(master_fn, test_fn):
    master_raster = arcpy.Raster(master_fn)
    test_raster = arcpy.Raster(test_fn)
    diff_raster = master_raster - test_raster
    assert diff_raster.minimum == 0 and diff_raster.maximum == 0, 'Pixel values do not match expected values.'

def compare_shp(master_fn, test_fn, fields='*'):
    master_data = [row for row in arcpy.da.SearchCursor(master_fn, fields)]
    test_data = [row for row in arcpy.da.SearchCursor(test_fn, fields)]
    assert test_data == master_data, 'Wrong data in the output shapefile.'

def has_fields(fn, fields):
    names = set([f.name for f in arcpy.ListFields(fn)])
    return names.intersection(fields) == fields
