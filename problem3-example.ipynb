{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Problem 3 example\n",
    "\n",
    "This example follows the general pattern of what you need to do for problem 3, but it uses a different type of data.\n",
    "\n",
    "**Instead of using points and distances, this uses numbers and differences. But the idea is the same. You will NOT use dictionaries or differences for your homework. You need to adapt the workflow to use a cursor and points instead.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First let's set up some sample data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = [dict(value=x) for x in [8, 1, 11, 6, 0, 16, 14, 4, 8, 14]]\n",
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a list of *dictionaries*. Dictionaries are objects that store data, like lists, but instead of using an index to get the value you want, you use a keyword. The dictionaries in our `data` variable only have one value each, which is accessible with the `'value'` keyword. But a dictionary can have lots of values, each accessible with a different keyword.\n",
    "\n",
    "For the example, we want to find the the difference between each value and the previous one, so the result looks something like this:\n",
    "\n",
    "```\n",
    "value  difference\n",
    " 8          0\n",
    " 1         -7\n",
    "11         10\n",
    " 6         -5\n",
    " 0         -6\n",
    "16         16\n",
    "14         -2\n",
    " 4        -10\n",
    " 8          4\n",
    "14          6\n",
    "```\n",
    "\n",
    "Here's a picture that shows how those difference values were calculated:\n",
    "\n",
    "![differences](images/difference3.jpg)\n",
    "\n",
    "The first item, with `value=8`, has a difference of 0 because there isn't an earlier item in the list. But the second one has a difference of -7 because `1 - 8 = -7`. The last one has a difference of 6 because `14 - 8 = 6`. Make sure you understand what we're after here, because you'll do the same thing for problem 3, except with distances between points."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's turn the `data` list into an *iterator* so that it behaves more like a *cursor*. This isn't really necessary, but I figured I'd make it as close to what you'd be doing as possible. This'll make it so you can only loop over the items in `data` and can't use an index to get any of them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cursor = iter(data)\n",
    "cursor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, so you can think of the `cursor` iterator as if it was your update cursor for problem 3.\n",
    "\n",
    "For problem 3, you want to calculate the distance between adjacent points. Here we'll calculate the difference between values of adjacent dictionaries in the iterator, instead. The first one has a difference of 0, so let's get it out of the iterator and set its difference to 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "item = next(cursor)\n",
    "item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use `next()` to get the first row in the update cursor for problem 3. However, it'll return a tuple, not a dictionary. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can get the number that goes with the 'value' keyword in the `item` dictionary like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "item['value']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use the 'difference' keyword to set a value of 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "item['difference'] = 0\n",
    "item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Think of this dictionary as the equivalent to a row that you'll get from the shapefile. The difference is that the row will be a tuple, not a dictionary, and you'll be interested in the geometries from the row instead of a 'value' item. The 'difference' here is similar to the 'distance' you'll create in the shapefile.\n",
    "\n",
    "We're done with `item` for now, but we'll need it in a minute in order to calculate the difference between it and the next dictionary in the list with a value of 1. Let's store it in a different variable that tells us it's the previously looked at value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "previous = item\n",
    "previous"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the real work starts. Let's loop through the rest of the items in `cursor` and for each one, find the difference between it and the previous one. The loop will start on the second item in the iterator, because using `next()` got the first one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for item in cursor:\n",
    "    \n",
    "    # Calculate the difference between this value and the previous one.\n",
    "    # This is where you'd calculate the distance between the current \n",
    "    # point and the previous one.\n",
    "    diff = item['value'] - previous['value']\n",
    "    \n",
    "    # Add the difference to the current item. This is where you'd update\n",
    "    # the distance field in the current row of your update cursor.\n",
    "    item['difference'] = diff\n",
    "    \n",
    "    # Put the current item into the previous variable so that it's \n",
    "    # available for the next iteration of the loop.\n",
    "    previous = item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see what's in the `data` list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's print it out a little nicer so that it's easier to see that each item contains the difference between its value and the previous value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('value\\tdifference')\n",
    "for item in data:\n",
    "    print('{value}\\t{difference}'.format(**item))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first difference is 0, but the second one is -7 because `1 - 8 = -7`. The third result is 10 because `11 - 1 = 10`, and so on. \n",
    "\n",
    "*If you're interested in my `format()` syntax there, it's something I didn't show you before. You can put keywords inside the {} placeholders, and then pass values to `format()` using keywords, like*\n",
    "\n",
    "```python\n",
    "'Hello {name}'.format(name='Bill')\n",
    "```\n",
    "\n",
    "*Using `**` with a dictionary expands it to keywords and values, just like using `*` expands a list (from the useful-tips notebook).*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But back to the homework. Think about how you'd apply this example to problem 3. \n",
    "\n",
    "1. Does the shapefile already have a distance field? If not, how can you solve that?\n",
    "2. What fields do you need to include when you create your update cursor? \n",
    "3. How do you get the point geometry out of each row of the update cursor? \n",
    "4. Which field do you update in the cursor?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
