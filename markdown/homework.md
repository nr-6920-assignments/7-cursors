# Cursors homework

*Notebooks are due Monday, March 8, at midnight*  
*Problems are due Monday, March 15, at midnight*

I'm giving you two weeks to do the problems for assignment, mostly because problem 3 really tends to trip people up. **TAKE ADVANTAGE OF THOSE TWO WEEKS AND DON'T WAIT UNTIL THE LAST MINUTE TO START THE ASSIGNMENT.**

## Notebook comprehension questions

*Worth 13 points*

Work through the notebooks in this order:

- 1-search-cursors.ipynb (6 points)
- 2-insert-cursors.ipynb (2 points)
- 3-update-cursors.ipynb (3 points)
- 4-mult-cursors (1 point)
- 5-useful-tips (1 point)

## Script problems

*Worth 10 points each*

## To get full credit:

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.

There are three problems, each explained in their own file.

- [Problem 1](problem1.md)
- [Problem 2](problem2.md)
- [Problem 3](problem3.md)
