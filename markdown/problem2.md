# To get full credit:

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.


# Problem 2

In this problem you're going to **use cursors** to create a new point shapefile using coordinates from a csv file.


## Part A

Create a notebook called `problem2`. Set up four variables in the first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\7-cursors\data'
input_filename = 'track.csv'
output_filename = 'track_points.shp'
srs = 'WGS 1984 UTM Zone 12N'
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

In order for Python to find the track.csv file, you'll want to add this line in addition to setting your arcpy workspace (this is because you're using the built-in Python `open()` function for the csv file, not arcpy).

```python
import os
os.chdir(folder)
```

If you're using ArcGIS and need to tell it how to find classtools, use this code:

```python
import sys
sys.path.append(os.path.dirname(folder))
```

Remember the track.csv file from last week? You used it to create a line geometry. This week you're going to use it to create a shapefile. Here's what the first few lines of the file look like:

```
id,date,x,y
302,20110402,473226.1307710044,4429257.122467113
303,20110402,472867.9297945726,4428476.929777273
304,20110402,472872.6206456687,4428487.89016115
```

- This notebook needs to create a new **point** shapefile file using the `output_filename` with the spatial reference contained in the `srs` variable (use the folder contained in the `folder` variable as the output location).
- The shapefile will need id and date fields, but not fields for x and y.
- Call your date field `date` because that's what my tests expect.
- These date values won't work with ArcGIS's DATE type, so make your date field be numeric-- I'd suggest LONG.
- Add a point feature to the shapefile for each row in the csv file.
- You can assume that the coordinates in the csv file use the same spatial reference as the `srs` variable.

Outputs that need to be in your notebook:

1. After creating your shapefile, use `Describe` to get its spatial reference and print it out. It should be `WGS 1984 UTM Zone 12N`.
2. Use `GetCount()` to make sure there are 147 rows in your new shapefile. 
3. Use classtools to plot your new shapefile.
4. Use a Search Cursor to print out the field names and all of the rows in the shapefile. Make sure you **get the field names from the Search Cursor** and not somewhere else. The first few rows will look like this:

```
(u'FID', u'Shape', u'Id', u'date')
(0, (473226.1307710044, 4429257.122467113), 302, 20110402)
(1, (472867.9297945726, 4428476.929777273), 303, 20110402)
(2, (472872.6206456687, 4428487.89016115), 304, 20110402)
(3, (473364.66166311613, 4429125.365658874), 305, 20110403)
```

Your plot should look like this:

![track_points.shp](images/track_points.png)

*Hint: Your code from part 1 of problem 2 from last week is the perfect starting point for this problem. But remember that you're not making a line anymore.*

*Another hint: You're also interested in the [Create Feature Class](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/create-feature-class.htm) and [Add Field](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/add-field.htm) tools. Remember that shapefiles automatically have an ID field, but you'll need to add the date field before trying to add any features to the new shapefile.*

*And yet another hint: What field name geometry [tokens](https://pro.arcgis.com/en/pro-app/arcpy/data-access/insertcursor-class.htm) make the most sense in this case, when you have x and y coordinates?*

*Oh, and more! This will be easier if you use a regular csv reader, not a DictReader. Take advantage of the fact that it'll give you lists of data for each row in the csv, and Insert Cursors also want lists of data. It makes it really easy to move data between the csv and shapefile, especially if you select the correct geometry tokens. The only reason you'll need more "work" code for this problem than for problem 1 is because you'll need to create a shapefile and add a date field to it. Other than that, you're just changing the direction that the data flows-- it's going from a csv into a shapefile instead of the other way around.*


## Part B

Now change these variables in the first cell (leave the other variables as they are):

```python
input_filename = 'sample_sites.csv'
output_filename = 'sample_points.shp'
srs = '4326'
```

Restart your notebook and run all of the cells again (choose Restart & Run All from the Kernel menu). If you have to change any code in your notebook other than the variables in the first cell, then you need to figure out why and fix it so that both examples run with the exact same code, just different variables. See how easy it is to import csv files with different spatial references without changing any code? If you were fancy, you could set something up so the csv files didn't have to have the same fields, but let's keep it simple for now.

This time sample_points.shp should have a spatial reference of `GCS_WGS_1984`, 42 points, and look like this:

```
(u'FID', u'Shape', u'Id', u'date')
(0, (-111.53608010030214, 41.92710756946264), 1, 20110402)
(1, (-111.60284898069213, 41.85454610864931), 2, 20110402)
(2, (-111.706891186305, 41.720894964863085), 3, 20110402)
(3, (-111.3815897801985, 41.62929274437927), 4, 20110403)
```

![sample_points.shp](images/sample_points.png)


## Turn in:
1. problem2.ipynb
