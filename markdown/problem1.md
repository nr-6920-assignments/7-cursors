
# Announcement

*I thought I'd put this here just to make sure you all saw it!*

1. The notebooks for this week are due on Monday the 8th, as usual. You have two weeks to do the problems, and they're due on the 15th. The reason I'm giving you more time is because students tend to find them difficult (especially problem 3), so don't wait until the last minute.
2. This class has a semester project, and I want you to write short proposals for what you'd like to do. These proposals are due March 15 (in two weeks). See the assignment description in Canvas for more info. 


# To get full credit for this week's problems

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.


# Problem 1

This problem has two notebooks because the second one is just the first one with a small change in order to make it more flexible. So you'll do the first one to figure out the general process, and then change it in the second one to make it more useful.

You're going to create a csv file that contains some of the attribute data from a shapefile.


## Part A

Create a notebook called `problem1a`. Set up three variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\7-cursors\data'
input_filename = 'cities.shp'
output_filename = 'cities1a.csv'
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

- This notebook needs to create a CSV file using the `output_filename`. 
- This CSV file needs to contain only the NAME, POPLASTCEN, and POPLASTEST fields for the cities in the `input_filename` shapefile that are county seats (the ones where COUNTYSEAT = 1). 
- Make sure you add a header row to your output file. Part 2 will be easier if you use the Search Cursor's `fields` property to get the values for the header row (even though you know what the field names are for this part of the problem).

Outputs that need to be in your notebook:

1. After creating the csv file, use `GetCount()` to make sure there are 29 rows. (Yep, you can use `GetCount()` with a csv file.)
2. Use a Search Cursor to print out the field names and all of the rows in the csv file. When you do this, make sure that you **get the field names from the Search Cursor** and not somewhere else. (Search Cursors can be used with csv files as well as spatial data. They're used exactly the same way, except that the geometry stuff doesn't work.) I want you to do this to practice using cursors some more.
3. Use either of these two methods to print the raw contents of your csv file. I want you to do this to **make sure that your csv file is single spaced**.

```python
# This method uses Python to print out the contents of the file.
with open(output_filename) as fp:
    for row in fp:
        print(row.strip())
```

```python
# This method uses the IPython shell command (`!`) to run a Windows 
# command line tool (`type`) to display the contents of the file.
# This is NOT Python code.
!type $output_filename
```

I know there are lots of ways to do this problem, but I want you to **use cursors**. Do not use the Table to Table geoprocessing tool.

Do not put paths or filenames anywhere in your notebook except for the first cell with the variables. Use the variables instead.

See the csv-files notebook from last week for examples on how to create csv files. Remember that you use a list or tuple full of data in order to write a row to a csv file, and search cursors give you a tuple for each feature. Because of this, your code can be *really* short because you can use the tuples from the cursor when writing to the csv file. You can do the "work" part of your code in only 5 or 6 lines, and two of those are just opening files.

### Expected output

The cities1a.csv file should have 30 lines in it (including the header line, (so `GetCount()` will say there are 29 because it doesn't count the header). The first few lines will look like this when printed with the code I gave you above or if you open it in Jupyter or another text editor:

```
NAME,POPLASTCEN,POPLASTEST
Beaver,2454,2904
Brigham City,17411,20055
Logan,42670,52776
Manila,308,316
```


## Part B

Make a copy of your `problem1a` notebook and rename it to `problem1b`.  Change the variables cell so that it has these five variables (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\7-cursors\data'
input_filename = 'cities.shp'
output_filename = 'cities1b.csv'
fields = '*'
where = None
```

- Change the notebook's code so that instead of getting the NAME, POPLASTCEN, and POPLASTEST fields, it gets the fields contained in the `fields` variable.
- Change the notebook's code so that instead of getting the features where COUNTYSEAT = 1, it gets the features specified with the `where` variable.
- All of the output expected in problem 1A should still be in the problem 1B notebook (but since you made a copy of the problem 1A notebook, it's already there, right?).

*Hint: You need to use the Search Cursor's `fields` property when writing the header row for the csv, otherwise you'll get a useless header row `fields` is set to `'*'`.*

*Another hint: `None` is a perfectly valid value for the `where_clause` parameter when creating a Search Cursor.*

### Expected output

When you run the notebook using the variables shown above, the cities1b.csv file should have 249 lines in it (including the header line, so `GetCount()` will report 248). The first few lines will look like this:

```
FID,Shape,COUNTYNBR,SHORTDESC,NAME,COUNTYSEAT,FIPS,POPLASTCEN,POPLASTEST,GNIS
0,"(324040.90773695963, 4251372.878255174)",01,MILFORD,Milford,0,50040,1451,1524,1430317
1,"(356642.0569623563, 4237582.455380205)",01,BEAVER,Beaver,1,4060,2454,2904,1438510
```


## Part C

Now change these variables in the first cell (leave the other variables as they are):

```python
output_filename = 'big_cities1b.csv'
fields = ['NAME', 'COUNTYSEAT', 'POPLASTCEN', 'POPLASTEST']
where = 'POPLASTEST > 100000'
```

Restart your notebook and run all of the cells again (choose Restart & Run All from the Kernel menu). If you have to change any code in your notebook other than the variables in the first cell, then you need to figure out why and fix it so that both examples run with the exact same code, just different variables. See how easy it is to get totally different results without changing any code except variables? That's a powerful tool!

### Expected output

The big_cities1b.csv file should have 5 lines in it (including the header line, so `GetCount()` will report 4). It'll look like this:

```
NAME,COUNTYSEAT,POPLASTCEN,POPLASTEST
West Valley City,0,108896,122003
Salt Lake City,1,181743,180086
West Jordan,0,68336,108204
Provo,1,105166,121330
```


## Turn in:
1. problem1a.ipynb
2. problem1b.ipynb
3. cities1a.ipnb
2. cities1b.csv
3. big_cities1b.csv
