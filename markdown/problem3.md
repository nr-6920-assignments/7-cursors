# To get full credit:

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.


# Problem 3

Say you had a set of GPS locations for an animal and you wanted to know how far the animal traveled in between each location. In this problem you're going to learn how to calculate that distance and add it to a point shapefile. The shapefile already exists, but you'll need to add a field called `distance` so that you have somewhere to put the distance values that you calculate. I drew a picture of what I mean. Pretend these are the points in the shapefile, and the numbers next to each point show their order in the shapefile. You want to calculate the distances between each point, so it's the length of the lines labeled with "d". The new field in the attribute table will have a 0 for the first point, but the calculated distances for the other points.

![points and lines](images/distance-lines1.jpg)
![attributes](images/distance-atts1.jpg)

## Directions

Create a notebook called `problem3`. Set up one variable in the first code cell, with this name and value (except change the path so it works on **your** computer:

```python
filename = r'D:\classes\NR6920\Assignments\7-cursors\data\track.shp'
```

There shouldn't be anything else in that first cell, and the filename should not appear anywhere else in your notebook. Always use the variables that you set here.

- There is no need to set a workspace because you have a full path to the file. Just use the `filename` variable as you always would, but without setting `arcpy.env.workspace` first.
- This notebook needs to add a floating point field called `distance` to the shapefile specified with the `filename` variable.
- Then use an Update Cursor to fill this field with the distance from the *previous point in the shapefile to the current point*. Since there is no previous point when you're looking at the first feature in the shapefile, then its distance value will be 0. 
- After updating all of the rows, use a search cursor to print out the id and distance for each row in the shapefile. The first few rows should look like this:

```
(u'id', u'distance')
(302, 0.0)
(303, 858.4920043945312)
(304, 11.92199993133545)
(305, 805.281982421875)
```

You'll need a variable to keep track of the previous point. You can get the point from the first feature in the shapefile and store it in this variable before starting to loop over the rest of the features. Then, inside your loop, use the [distanceTo()](https://pro.arcgis.com/en/pro-app/arcpy/classes/geometry.htm) geometry method to get the distance between the current row's point and the previous point. Make sure you store the current row or point in the "previous point" variable before going to the next iteration of your loop. Otherwise, you'll be calculating the distance to the first point in the shapefile every time.

If you're confused, take a look at [these slides](https://docs.google.com/presentation/d/1Oj88un6575vLOcCMXLtkKyqrwNbTG3vWJFWdrfivaYA/edit?usp=sharing) and hopefully they'll help. You can also work through the `problem3-example` notebook in order to get the workflow- just **DO NOT TRY TO USE THE SAME DATA TYPES FOR YOUR HOMEWORK**. The notebook has almost all of the steps you need-- you just need to **adapt it to use points and distances**.

If you mess up and want to delete your distance field, you can use [Delete Field](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/delete-field.htm) or Github Desktop by right-clicking on the modified .dbf file and choosing Discard.

## Turn in:
- problem3.ipynb
