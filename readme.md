# Cursors

This week you'll learn how to work with a single feature in a feature class at a time, rather than using a geoprocessing tool to operate on the entire feature class.

See the [homework description](markdown/homework.md).
