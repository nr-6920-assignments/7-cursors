{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets for this week.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 7-cursors\\data folder.\n",
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\7-cursors\\data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import arcpy\n",
    "\n",
    "arcpy.env.workspace = data_folder"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reading and writing vector data with ArcPy\n",
    "\n",
    "While you can do a lot of things using the geoprocessing tools in ArcGIS, and using ArcPy to combine these tools and automate workflows is extremely powerful, there are still times when you want to do something that isn't possible with the built-in tools. That's why you learned how to create your own geometries last week, but you also need to know how to read, write, and manipulate **individual** features in your feature class.\n",
    "\n",
    "ArcPy has the concept of *cursors* to help you with this. One thing you need to be aware of is that there are two types of cursors in ArcPy, and they are not interchangeable. We'll only look at the newer kind because they provide much better performance than the old ones. These new ones live in the [Data Access module](https://pro.arcgis.com/en/pro-app/arcpy/data-access/what-is-the-data-access-module-.htm) (`arcpy.da`). You need to use `arcpy.da` to create these cursors, and if you see code that creates cursors without using the `.da` part of it, then that code is using the old cursors. \n",
    "\n",
    "If you need to learn how the old cursors work, the documentation is [here](https://pro.arcgis.com/en/pro-app/arcpy/classes/cursor.htm), but **don't use them unless you have to** (if you only have an ancient version of ArcGIS, for example). I thought Esri might get rid of the old style with ArcGIS Pro, but they kept them around so that old scripts would continue to work.\n",
    "\n",
    "There are three kinds of cursors:\n",
    "\n",
    "1. SearchCursor: Allows you to read features but not change them or add any new features.\n",
    "2. UpdateCursor: Allows you to read, update, and delete existing features, but not add any new features.\n",
    "3. InsertCursor: Allows you to add new features only."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# SearchCursors\n",
    "\n",
    "**A SearchCursor allows you to loop through features one at a time and READ their data. Think of it as looping through each row in an attribute table.**\n",
    "\n",
    "In order to create a [SearchCursor](https://pro.arcgis.com/en/pro-app/arcpy/data-access/searchcursor-class.htm), you need to provide the path to the feature class (`in_table`) and a list of fields that you want retrieved (`field_names`). If you want to get all fields, just use `'*'` instead of a list of field names. There are other optional parameters, but those two are required.\n",
    "\n",
    "```python\n",
    "SearchCursor(in_table, field_names, {where_clause}, {spatial_reference}, \n",
    "    {explode_to_points}, {sql_clause}, {datum_transformation})\n",
    "```\n",
    "\n",
    "The SearchCursor will give you an *iterator* of tuples. An iterator is something you can loop through, but cannot pull random items out of. For example, with a list, you could use `my_list[2]` to get the third item in the list. You can't do this with an iterator; you can only loop through it.\n",
    "\n",
    "This creates a SearchCursor that uses `'*'` to tell it to read all of the fields in the attribute table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor for all of the fields (*) in cache_cities.shp\n",
    "searcher = arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names='*')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you used `'*'` to read all of the fields in the attribute table, you might want to know the names of those fields. The `fields` property returns a list of field names in the same order they're included in the cursor's dataset. Let's get the fields included in the cursor we just opened:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the list of field names\n",
    "searcher.fields"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's loop through the features in the search cursor (so cache_cities.shp) and print out data contained in each of the attribute table fields. This does the same thing as the `for` loops you saw earlier, except that it's looping through the rows of data contained in the cursor instead of a list of numbers or strings. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Loop through the rows of data in the search cursor and print them out\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each row of the cursor is a *tuple* containing data for each of the attribute fields you printed out earlier. (Remember that a tuple is like a list, but can't be changed.) The data values are in the same order as the field names, so since the sixth field in the list of names is SHORTDESC, then the sixth value printed for each row is the short description for the city.\n",
    "\n",
    "Here's the first row of data matched up with field names:\n",
    "\n",
    "```\n",
    "FID             0\n",
    "Shape           (431699.5127824595, 4615141.792413884)\n",
    "COUNTYNBR       03\n",
    "ENTITYNBR       3090.0\n",
    "ENTITYYR        2009.0\n",
    "SHORTDESC       MILLVILLE\n",
    "SDEKEY          2009033090\n",
    "NAME            Millville\n",
    "IMSCOLOR        2\n",
    "MINNAME          \n",
    "COUNTYSEAT      0\n",
    "FIPS            50370\n",
    "POPLASTCEN      1507\n",
    "POPLASTEST      2027\n",
    "GNIS            1443496\n",
    "SHAPE_Leng      19596.5676238\n",
    "SHAPE_Area      5837900.56\n",
    "```\n",
    "\n",
    "Notice that the value for the Shape field is a tuple containing a set of x,y values. The cursor doesn't return the actual geometry object unless you ask for it. Instead, it returned the coordinates for the polygon's centroid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Limiting fields\n",
    "\n",
    "The last example retrieved all of the attributes because you passed `'*'` as the second parameter when creating your search cursor, which tells it that you want all of the columns. If you only want a few of the fields, you can use a field list when creating the cursor instead. This is actually a really a good idea if you're using a large dataset because it will go a lot faster if you only request the fields you need.\n",
    "\n",
    "Let's get the NAME and POPLASTCEN (population last census) fields. Now the cursor only knows about those two fields when you print out the list of field names:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor for the NAME and POPLASTCEN fields in cache_cities.shp\n",
    "searcher = arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names=['NAME', 'POPLASTCEN'])\n",
    "\n",
    "# Get the list of field names\n",
    "searcher.fields"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And those are also the only fields that print out when you print the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Loop through the rows of data in the search cursor and print them out\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The fields are returned in the same order you specify them, so you could reverse the order like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor with the same fields but a different order and print them out\n",
    "searcher = arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names=['POPLASTCEN', 'NAME'])\n",
    "print(searcher.fields)\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since each feature is returned as a tuple and you can grab values out of tuples using their index (just like a list), you can pull out individual values using their index. NAME is at index 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Go back to the beginning of the cursor\n",
    "searcher.reset()\n",
    "\n",
    "# Print out the name for each row\n",
    "for row in searcher:\n",
    "    print(row[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that you called `searcher.reset()` before you looped through the rows. That's because once you loop through an iterator, it's done and can't be looped over again unless you tell it to go back to the beginning using `reset()`. If you try looping through again without resetting, nothing will happen, like here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Nothing happens because we've already looped through the\n",
    "# cursor once and it's at its end. It won't automatically\n",
    "# go back to the beginning.\n",
    "for row in searcher:\n",
    "    print(row[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 1\n",
    "\n",
    "Use a search cursor to load **only the ID and COVER fields** from **sites.shp**. Print out the data for each row. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Geometries and tokens\n",
    "\n",
    "Remember how the centroid coordinates were returned for the Shape field earlier? There are special *tokens* you can use to retrieve actual geometry objects or their properties. For example, **`'SHAPE@'` will get you the actual geometry object** and `'SHAPE@XY'` will return a tuple containing the centroid x and y coordinates. You can see a full list of tokens in the [SearchCursor documentation](https://pro.arcgis.com/en/pro-app/arcpy/data-access/searchcursor-class.htm) (they're in the Syntax section, listed under the `field_names` parameter).\n",
    "\n",
    "Let's get the geometry for each city and print out its area (also notice that I didn't bother to use all caps this time-- field names aren't case-sensitive):\n",
    "\n",
    "- `row[0]`: This is the NAME attribute because `'name'` is the first value in the `field_names` parameter.\n",
    "- `row[1]`: This is the geometry object because `'shape@'` is the second value in the `field_names` parameter. You can get the geometry's area with `row[1].area`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor for the name attribute and the geometry object (shape@)\n",
    "searcher = arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names=['name', 'shape@'])\n",
    "\n",
    "# Print out the field names\n",
    "print(searcher.fields)\n",
    "\n",
    "# Print out the name and the geometry's area for each row\n",
    "for row in searcher:\n",
    "    print(row[0], row[1].area)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If all you need is the area of the polygon, though, it's faster to use a token to get it, instead of getting the entire geometry and then calculating the area. You can retrieve the area instead of the actual geometry with `'shape@area'`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use shape@area to get the geometry's area (which is a number) \n",
    "# instead of the actual shape (which is a geometry object)\n",
    "searcher = arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names=['name', 'shape@area'])\n",
    "\n",
    "# Print out the field names\n",
    "print(searcher.fields)\n",
    "\n",
    "# Print out the name and the geometry's area for each row. Now we can use the *-operator.\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 2\n",
    "\n",
    "The sites.shp dataset contains points. Copy your answer from problem 1 and then change it so it also includes the x and y values as **separate fields (NOT an (x,y) tuple)** for each point (keep the ID and COVER fields).  Look at the [documentation](https://pro.arcgis.com/en/pro-app/arcpy/data-access/searchcursor-class.htm) to figure out which geometry tokens to use. Again, print out the data for all rows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Limiting by attribute\n",
    "\n",
    "What if you only want some of the features? You could of course use an `if-statement` to check if a feature was one you wanted while you looped through, but an easier (and faster!) way is to only get the ones you're interested in to begin with. To do this, use the optional `where_clause` parameter when you create your SearchCursor. This parameter is like the selection criteria you used two weeks ago when learning about selection sets. You can read more about building a `where` clause here: [SQL reference for query expressions used in ArcGIS](https://pro.arcgis.com/en/pro-app/help/mapping/navigation/sql-reference-for-elements-used-in-query-expressions.htm)\n",
    "\n",
    "This `where_clause` would only read the cities with a population greater than 5000:\n",
    "\n",
    "```sql\n",
    "poplastcen > 5000\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor for the cities with a population > 5000\n",
    "searcher = arcpy.da.SearchCursor(\n",
    "    in_table='cache_cities.shp', \n",
    "    field_names=['name', 'poplastcen'], \n",
    "    where_clause='poplastcen > 5000',\n",
    ")\n",
    "\n",
    "# Print out the field names\n",
    "print(searcher.fields)\n",
    "\n",
    "# Print out each row of data\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When matching strings, you need single-quotes around the string you want to match. So to get Logan, you would need a query that looked like this:\n",
    "\n",
    "```sql\n",
    "name = 'Logan'\n",
    "```\n",
    "\n",
    "You can do that by surrounding your entire string with double quotes, like in the following example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor to find rows where name = 'Logan'\n",
    "searcher = arcpy.da.SearchCursor(\n",
    "    in_table='cache_cities.shp', \n",
    "    field_names=['name', 'poplastcen'], \n",
    "    where_clause=\"name = 'Logan'\",\n",
    ")\n",
    "\n",
    "# Print out each row of data\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two things to be aware of here:\n",
    "1. SQL uses a single = to check for equality.\n",
    "2. Although field names are not case-sensitive (you could use \"name\" or \"NAME\"), the string you're matching is, so matching against 'logan' instead of 'Logan' wouldn't find anything.\n",
    "\n",
    "You can also match partial strings by using `LIKE` instead of `=` and using `%` as a wildcard that will match anything.\n",
    "\n",
    "For example, this query would find all features with \"Logan\" anywhere in the name because the percent signs would match anything before or after \"Logan\":\n",
    "\n",
    "```sql\n",
    "name LIKE '%Logan%'\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor to find rows where 'Logan' is in the name\n",
    "searcher = arcpy.da.SearchCursor(\n",
    "    in_table='cache_cities.shp', \n",
    "    field_names=['name', 'poplastcen'], \n",
    "    where_clause=\"name LIKE '%Logan%'\",\n",
    ")\n",
    "\n",
    "# Print out each row of data\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This query would find anything that starts with \"Logan\" because the percent sign would match anything after \"Logan\", but there's no percent sign at the beginning to match characters before \"Logan\" (so North Logan would no longer match):\n",
    "\n",
    "```sql\n",
    "name LIKE 'Logan%'\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor to find rows where the name starts with 'Logan'\n",
    "searcher = arcpy.da.SearchCursor(\n",
    "    in_table='cache_cities.shp', \n",
    "    field_names=['name', 'poplastcen'], \n",
    "    where_clause=\"name LIKE 'Logan%'\",\n",
    ")\n",
    "\n",
    "# Print out each row of data\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 3\n",
    "\n",
    "Copy your answer from problem 2 and change it so that it only reads in the rows that have a cover type of 'grass'. Keep all of the fields from problem 2. Again, print out the values of the rows. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sorting your results\n",
    "\n",
    "You can use the optional `sql_clause` parameter to sort your rows if you're using a geodatabase, but not with shapefiles. Since this notebook is on search cursors, I'll show you the search cursor method (`sql_clause`) first, even though it doesn't work with shapefiles. Then I'll show you a method that works with all data.\n",
    "\n",
    "### Using `sql_clause`\n",
    "\n",
    "Let's look at how to use the `sql_clause` parameter to sort results from a file geodatabase. There's a geodatabase in your data folder called sample.gdb, and it contains a feature class called cities. First let's print out the name and poplastcen fields in the cities feature class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "searcher = arcpy.da.SearchCursor(in_table='sample.gdb/cities', field_names=['name', 'poplastcen'])\n",
    "print(searcher.fields)\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's sort by name. The `sql_clause` parameter needs to be a tuple where the first item is a prefix clause and the second is a postfix clause. Don't worry about that for now, except know that we're not going to use a prefix clause, so the first item in the tuple will be `None`. Our postfix clause will tell it to order by name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "searcher = arcpy.da.SearchCursor(\n",
    "    in_table='sample.gdb/cities', \n",
    "    field_names=['name', 'poplastcen'], \n",
    "    sql_clause=(None, 'ORDER BY name'),\n",
    ")\n",
    "print(searcher.fields)\n",
    "for row in searcher:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using `sorted()`\n",
    "\n",
    "While the `sql_clause` parameter works inside the geodatabase, using the Python `sorted` function works on the data after it's been returned from the dataset. Because of that, it works for everything, although if you're using a geodatabase you'll get better performance by using `sql_clause`. Note that `sorted()` is a Python function, not an ArcPy function. You can use it to sort a list containing any kind of data.\n",
    "\n",
    "Here's an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor for the name and poplastcen fields\n",
    "searcher = arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names=['name', 'poplastcen'])\n",
    "\n",
    "# Loop through the sorted rows (by name) in the search cursor\n",
    "for row in sorted(searcher):\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using `sorted` sorts by the first thing in the tuple (in this case, name). If you want to sort by something else, just rearrange the columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor for the poplastcen and name fields\n",
    "searcher = arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names=['poplastcen', 'name'])\n",
    "\n",
    "# Loop through the sorted rows (by poplastcen) in the search cursor\n",
    "for row in sorted(searcher):\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cleaning things up\n",
    "\n",
    "Python does a pretty good job of cleaning things up when the Python session ends, but you probably won't be ending your session as soon as you're done with a file, so you might run into trouble if you don't close it.\n",
    "\n",
    "Cursors support a `with` syntax that automatically closes them when you're done with them (well, theoretically anyway-- Esri doesn't always implement this correctly so it doesn't always work, but syntax like this is used throughout Python and is *supposed* to mean that the resource is automatically closed). To use this, start your statement with the `with` keyword and end it with the variable name for the cursor, and then indent everything that needs to use the cursor under the `with` statement. This is the same syntax that you've used with the Python `open()` function in the past."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use 'with' to create a search cursor called searcher\n",
    "with arcpy.da.SearchCursor(in_table='cache_cities.shp', field_names=['poplastcen', 'name']) as searcher:\n",
    "    \n",
    "    # Loop through the rows and print out the data\n",
    "    for row in searcher:\n",
    "        print(row)\n",
    "\n",
    "# Any code out here, that's not indented under the 'with', can't use searcher"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The only reason I haven't used this syntax so far in the notebook was because you can't break code up into different cells when you do it this way. You should use this syntax whenever you can, however, and I'll try to use it for the rest of the examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 4\n",
    "\n",
    "Copy your answer for **problem 3** and change it so that it uses the `with` syntax you just read about."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Projecting geometries while reading\n",
    "\n",
    "One of the optional parameters when creating a `SearchCursor` is `spatial_reference`. This allows you to change the spatial reference of the geometries as you read them in. **This only works if the feature class knows what spatial reference it uses**, because it's impossible to reproject something if you don't know what projection it is to begin with. \n",
    "\n",
    "For example, let's look at the coordinates in the `sites.shp` point shapefile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor for sites.shp that gets the x and y coordinates\n",
    "with arcpy.da.SearchCursor(in_table='sites.shp', field_names=['shape@x', 'shape@y']) as searcher:\n",
    "    \n",
    "    # Loop through the rows and print them out\n",
    "    for row in searcher:\n",
    "        print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Those look like UTM coordinates to me, and you can always verify by getting the shapefile's spatial reference info:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arcpy.Describe('sites.shp').spatialReference.name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To convert them to a different spatial reference while reading them in, provide the desired spatial reference to the search cursor when you create it. Let's tell the search cursor to convert the points to lat/lon (WGS 1984) while reading them in:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor that converts geometries to WGS84\n",
    "with arcpy.da.SearchCursor(\n",
    "    in_table='sites.shp', field_names=['shape@x', 'shape@y'], spatial_reference='WGS 1984') as searcher:\n",
    "    \n",
    "    # Loop through the rows and print them out\n",
    "    for row in searcher:\n",
    "        print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you've got latitudes and longitudes!\n",
    "\n",
    "If you read in the actual geometry objects using the `SHAPE@` token, the geometries know what spatial reference they use. For example, this next code sample gets the spatial reference of the first point in sites.shp *without* reprojecting on-the-fly. You'll see that the point knows it uses UTM coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor that gets the geometry objects\n",
    "with arcpy.da.SearchCursor(in_table='sites.shp', field_names='shape@') as searcher:\n",
    "    \n",
    "    # Get the first row (next() gets the next row in line)\n",
    "    row = next(searcher)\n",
    "    \n",
    "    # Get the geometry (it's the only thing in the row's tuple and has index 0)\n",
    "    geom = row[0]\n",
    "    \n",
    "    # Print out the geometry's spatial reference name\n",
    "    print(geom.spatialReference.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `next` function just gets the next item in the iterator, so in this case it gets the first one that would be returned if you were to use a `for` loop to iterate over the rows.\n",
    "\n",
    "Now let's change the spatial reference on-the-fly by using the `spatial_reference` parameter so that the point uses lat/lon:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a search cursor that gets the geometry objects as WGS84\n",
    "with arcpy.da.SearchCursor(in_table='sites.shp', field_names='shape@', spatial_reference='WGS 1984') as searcher:\n",
    "    \n",
    "    # Get the first geometry\n",
    "    geom = next(searcher)[0]\n",
    "    \n",
    "    # Print out the geometry's spatial reference name\n",
    "    print(geom.spatialReference.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both of those last two examples used the same shapefile, but you were able to read the geometries using different spatial references."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 5\n",
    "\n",
    "Copy your answer for problem 4 and change it so that it reads the coordinates as web mercator (factory code is 3857). The first 3 rows of your printout should look like this:\n",
    "\n",
    "```\n",
    "(4, 'grass', -12398941.85807698, 5105610.035057268)\n",
    "(10, 'grass', -12367960.962389678, 5142002.681792077)\n",
    "(11, 'grass', -12379165.60368857, 5151222.385284245)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
