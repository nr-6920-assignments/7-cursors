import os
from tests.testtools import BaseTester, Runner

__unittest = True

def test_results(problem):
    if problem == '1A':
        Runner.test_output(problem, Problem1AR)
    elif problem == '1B':
        Runner.test_output(problem, Problem1BR)
    if problem == '1C':
        Runner.test_output(problem, Problem1CR)
    elif problem == '2A':
        Runner.test_output(problem, Problem2AR)
    elif problem == '2B':
        Runner.test_output(problem, Problem2BR)
    elif problem == 3:
        Runner.test_output(3, Problem3R)

def test_notebook(problem):
    if problem == '1A':
        Runner.run_problem(problem, [Problem1A])
    elif problem == '1B':
        Runner.run_problem(problem, [Problem1B, Problem1C, Problem1D])
    elif problem == 2:
        Runner.run_problem(2, [Problem2A, Problem2B, Problem2C])
    elif problem == 3:
        Runner.run_problem(3, [Problem3])


class CursorTester(BaseTester):

    def test_rows(self):
        """Test number of output rows"""
        self.notebook_ran()
        self.result_exists()
        result_df, expected_df = self._get_data()
        result_n, expected_n = len(result_df), len(expected_df)
        self.assertEqual(result_n, expected_n, f'Output has {result_n} rows instead of {expected_n}')

    def test_columns(self):
        """Test columns"""
        self.notebook_ran()
        self.result_exists()
        result_df, expected_df = self._get_data()
        result_cols, expected_cols = result_df.columns, expected_df.columns
        self.assertEqual(set(result_cols), set(expected_cols), f'Output columns are {result_cols} instead of {expected_cols}')

    def test_data(self):
        """Test data values"""
        self.notebook_ran()
        self.result_exists()
        result_df, expected_df = self._get_data()
        self.assertTrue(result_df.equals(expected_df), f'Output data appears to be incorrect')


class Problem1(CursorTester):
    problem = '1'
    args = {}
    in_filename = 'cities_test.shp'
    fields = '*'
    where = None

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder=self.data_folder,
            input_filename=self.in_filename,
            output_filename=self.result_filename,
            fields=self.fields,
            where=self.where,
        )
        self.expected_path = os.path.join(self.test_folder, 'data', self.expected_filename)
        self.result_path = os.path.join(self.data_folder, self.result_filename)

    def _get_data(self):
        import pandas as pd
        result_df = pd.read_csv(self.result_path).round(2).rename(columns=str.lower)
        expected_df = pd.read_csv(self.expected_path).round(2).rename(columns=str.lower)
        return result_df, expected_df


class Problem1A(Problem1):
    problem = '1a'
    suffix = ''
    result_filename = 'p1a.csv'
    expected_filename = 'p1a_data.csv'


class Problem1B(Problem1):
    problem = '1b'
    suffix = ''
    result_filename = 'p1b.csv'
    expected_filename = 'p1b_data.csv'


class Problem1C(Problem1):
    problem = '1b'
    suffix = '-c'
    result_filename = 'p1c.csv'
    expected_filename = 'p1c_data.csv'
    fields = ['NAME', 'COUNTYSEAT', 'POPLASTCEN', 'POPLASTEST']
    where = 'POPLASTEST > 100000'


class Problem1D(Problem1):
    problem = '1b'
    suffix = '-d'
    result_filename = 'p1d.csv'
    expected_filename = 'p1d_data.csv'
    in_filename = 'county_test.shp'
    fields=['NAME', 'STATEPLANE']
    where="STATEPLANE = 'North'"


class Problem1AR(Problem1A):
    result_filename = 'cities1a.csv'


class Problem1BR(Problem1B):
    result_filename = 'cities1b.csv'


class Problem1CR(Problem1C):
    result_filename = 'big_cities1b.csv'


class Problem2(CursorTester):
    problem = 2
    args = {}
    input_filename = ''
    srs = ''

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder=self.data_folder,
            input_filename=self.input_filename,
            output_filename=self.result_filename,
            srs=self.srs,
        )
        self.expected_path = os.path.join(self.test_folder, 'data', self.expected_filename)
        self.result_path = os.path.join(self.data_folder, self.result_filename)


    def _get_data(self):
        import pandas as pd
        import arcpy
        standard_fields = ['fid', 'shape', 'id']
        fields = [x.name.lower() for x in arcpy.ListFields(self.result_path)]
        extra_fields = list(set(fields) - set(standard_fields))
        with arcpy.da.SearchCursor(self.result_path, ['id', extra_fields[0], 'shape@x', 'shape@y']) as rows:
            result_df = pd.DataFrame(data=list(rows), columns=rows.fields).round(2).rename(columns=str.lower)
        expected_df = pd.read_csv(self.expected_path).round(2).rename(columns=str.lower)
        return result_df, expected_df


class Problem2A(Problem2):
    suffix = 'a'
    input_filename = 'track_test.csv'
    result_filename = 'p2a_check.shp'
    srs = 'WGS 1984 UTM Zone 12N'
    expected_filename = 'p2a_data.csv'


class Problem2B(Problem2):
    suffix = 'b'
    input_filename = 'sites_test.csv'
    result_filename = 'p2b_check.shp'
    srs = 4326
    expected_filename = 'p2b_data.csv'


class Problem2C(Problem2):
    suffix = 'c'
    input_filename = 'track_test2.csv'
    result_filename = 'p2c_check.shp'
    srs = 'WGS 1984 UTM Zone 12N'
    expected_filename = 'p2c_data.csv'


class Problem2AR(Problem2A):
    result_filename = 'track_points.shp'


class Problem2BR(Problem2B):
    result_filename = 'sample_points.shp'


class Problem3(CursorTester):
    problem = 3
    args = {}
    result_filename = 'track_test.shp'
    expected_filename = 'p3_data.csv'

    def setUp(self):
        super().setUp()
        self.args = dict(
            filename = os.path.join(self.data_folder, self.result_filename),
        )
        self.expected_path = os.path.join(self.test_folder, 'data', self.expected_filename)
        self.result_path = os.path.join(self.data_folder, self.result_filename)

    def _get_data(self):
        import pandas as pd
        import arcpy
        with arcpy.da.SearchCursor(self.result_path, ['id', 'distance']) as rows:
            result_df = pd.DataFrame(data=list(rows), columns=rows.fields).round(2).rename(columns=str.lower)
        expected_df = pd.read_csv(self.expected_path).round(2).rename(columns=str.lower)
        return result_df, expected_df


class Problem3R(Problem3):
    result_filename = 'track.shp'
